package cz.siska.messenger.chatrooms

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.siska.messenger.models.ChatRoom
import cz.siska.messenger.network.Api
import kotlinx.coroutines.launch

class ListViewModel : ViewModel() {
    private val _chatRooms = MutableLiveData<List<ChatRoom>>()
    private val _navigateToSelectedChatRoom = MutableLiveData<ChatRoom>()

    val chatRooms: LiveData<List<ChatRoom>>
        get() = _chatRooms

    val navigateToSelectedChatRoom: LiveData<ChatRoom>
        get() = _navigateToSelectedChatRoom

    init {
        refreshChatRooms()
    }

    fun refreshChatRooms() {
        viewModelScope.launch {
            try {
                _chatRooms.value = Api.retrofitService.getAllChatRooms().data
            } catch (e: Exception) {
                _chatRooms.value = ArrayList()
            }
        }
    }

    fun displayChatRoomMessages(chatRoom: ChatRoom) {
        _navigateToSelectedChatRoom.value = chatRoom
    }

    fun displayChatRoomMessagesComplete() {
        _navigateToSelectedChatRoom.value = null
    }
}
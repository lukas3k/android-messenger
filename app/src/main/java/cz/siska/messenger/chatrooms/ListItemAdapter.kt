package cz.siska.messenger.chatrooms

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.siska.messenger.databinding.ListItemBinding
import cz.siska.messenger.models.ChatRoom
import kotlinx.android.synthetic.main.list_item.view.*

class ListItemAdapter(private val onClickListener: OnClickListener) :
    ListAdapter<ChatRoom, ListItemAdapter.ChatRoomViewHolder>(DiffCallback) {

    class ChatRoomViewHolder(private var binding: ListItemBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(chatRoom: ChatRoom) {
            binding.chatRoom = chatRoom
            binding.titleFirstLetter = chatRoom.title.first().toString()
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<ChatRoom>() {
        override fun areItemsTheSame(oldItem: ChatRoom, newItem: ChatRoom): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: ChatRoom, newItem: ChatRoom): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ChatRoomViewHolder {
        return ChatRoomViewHolder(ListItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ChatRoomViewHolder, position: Int) {
        val chatRoom = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(chatRoom)
        }

        holder.bind(chatRoom)
        holder.itemView.tvIcon.background.setTint(Color.parseColor("#" + chatRoom.color))
    }
}

class OnClickListener(val clickListener: (chatRoom: ChatRoom) -> Unit) {
    fun onClick(chatRoom: ChatRoom) = clickListener(chatRoom)
}
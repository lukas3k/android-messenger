package cz.siska.messenger.chatrooms

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import cz.siska.messenger.MessengerApplication
import cz.siska.messenger.R
import cz.siska.messenger.databinding.FragmentListBinding
import cz.siska.messenger.storage.SharedPrefManager
import cz.siska.messenger.ui.LoginActivity
import cz.siska.messenger.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_list.view.*

class ListFragment : Fragment() {

    private val viewModel: ListViewModel by lazy {
        ViewModelProvider(this).get(ListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val binding = FragmentListBinding.inflate(inflater)
        val actionBar = ((activity as MainActivity).supportActionBar as ActionBar)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.listItems.adapter = ListItemAdapter(OnClickListener {
            viewModel.displayChatRoomMessages(it)
        })

        actionBar.title = "Seznam místností"
        actionBar.setDisplayHomeAsUpEnabled(false)

        viewModel.navigateToSelectedChatRoom.observe(viewLifecycleOwner, {
            if ( it != null ) {
                this.findNavController().navigate(ListFragmentDirections.actionShowMessages(it))
                viewModel.displayChatRoomMessagesComplete()
            }
        })

        binding.root.swipeLayout.setOnRefreshListener {
            viewModel.refreshChatRooms()
            binding.root.swipeLayout.isRefreshing = false
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.overflow_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.logout) {
            val prefManager = SharedPrefManager.getInstance(MessengerApplication.instance)
            prefManager.clear()

            val intent = Intent(MessengerApplication.instance, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }
}
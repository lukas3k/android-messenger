package cz.siska.messenger

import android.app.Application
import timber.log.Timber

class MessengerApplication : Application() {
    companion object {
        lateinit var instance: MessengerApplication
    }

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}

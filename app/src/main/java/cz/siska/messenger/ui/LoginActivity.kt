package cz.siska.messenger.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import cz.siska.messenger.R
import cz.siska.messenger.models.Login
import cz.siska.messenger.models.ResponseData
import cz.siska.messenger.models.User
import cz.siska.messenger.network.Api
import cz.siska.messenger.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val prefManager = SharedPrefManager.getInstance(this)
        if (prefManager.isLoggedIn) {
            goToMessageList()
            return
        }

        buttonLogin.setOnClickListener {
            val email = editTextEmail.text.toString().trim()
            val password = editTextPassword.text.toString().trim()

            if (email.isEmpty()) {
                editTextEmail.error = "Email je povinný"
                editTextEmail.requestFocus()
                return@setOnClickListener
            }

            if (password.isEmpty()) {
                editTextPassword.error = "Heslo je povinné"
                editTextPassword.requestFocus()
                return@setOnClickListener
            }

            Api.retrofitService.login(Login(email, password))
                .enqueue(object: Callback<ResponseData<User>> {
                    override fun onResponse(
                        call: Call<ResponseData<User>>,
                        response: Response<ResponseData<User>>
                    ) {
                        when {
                            response.isSuccessful -> {
                                SharedPrefManager
                                    .getInstance(applicationContext)
                                    .saveUser(response.body()?.data!!)

                                goToMessageList()
                            }
                            response.code() == 401 -> {
                                Toast.makeText(
                                    applicationContext,
                                    "Neplatné přihlašovací údaje",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                            else -> {
                                Toast.makeText(
                                    applicationContext,
                                    "Nepovedlo se přihlásit, akci opakujte znovu",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseData<User>>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            t.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }

                })
        }
    }

    private fun goToMessageList() {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}
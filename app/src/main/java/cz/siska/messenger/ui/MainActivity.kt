package cz.siska.messenger.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cz.siska.messenger.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
package cz.siska.messenger.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import cz.siska.messenger.models.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

private const val BASE_URL = "https://messenger-backend.siska.dev"

private val moshi = Moshi
    .Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val httpClient = OkHttpClient
    .Builder()
    .addInterceptor(AuthInterceptor())
    .build()

private val retrofit = Retrofit
    .Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .client(httpClient)
    .build()

interface ApiService {
    @POST("/api/v1/user/login")
    fun login(@Body request: Login): Call<ResponseData<User>>

    @GET("/api/v1/chat-room")
    suspend fun getAllChatRooms(): ResponseData<List<ChatRoom>>

    @GET("/api/v1/chat-room/{chatRoomId}/message")
    suspend fun getAllMessages(@Path("chatRoomId") chatRoomId: Int): ResponseData<List<Message>>

    @POST("/api/v1/chat-room/{chatRoomId}/message")
    fun createMessage(@Path("chatRoomId") chatRoomId: Int, @Body request: MessageCreate): Call<ResponseData<Message>>
}

object Api {
    val retrofitService: ApiService by lazy { retrofit.create(ApiService::class.java) }
}
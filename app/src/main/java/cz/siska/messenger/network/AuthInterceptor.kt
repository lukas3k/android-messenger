package cz.siska.messenger.network

import cz.siska.messenger.MessengerApplication
import cz.siska.messenger.storage.SharedPrefManager
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        val prefManager = SharedPrefManager.getInstance(MessengerApplication.instance)

        requestBuilder.addHeader("Authorization", "Basic " + prefManager.user.apiKey)

        return chain.proceed(requestBuilder.build())
    }
}
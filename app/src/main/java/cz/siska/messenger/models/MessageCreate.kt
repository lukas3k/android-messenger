package cz.siska.messenger.models

data class MessageCreate(
    val content: String
)
package cz.siska.messenger.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChatRoom(
    val id: Int,
    val title: String,
    val description: String,
    val color: String
) : Parcelable
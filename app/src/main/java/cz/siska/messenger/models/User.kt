package cz.siska.messenger.models

data class User(
    val id: Int,
    val name: String,
    val surname: String,
    val email: String,
    val color: String,
    val apiKey: String = ""
)
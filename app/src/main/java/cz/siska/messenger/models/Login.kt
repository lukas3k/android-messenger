package cz.siska.messenger.models

data class Login(
    val email: String,
    val password: String
)
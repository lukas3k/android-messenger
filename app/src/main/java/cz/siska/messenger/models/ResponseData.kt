package cz.siska.messenger.models

data class ResponseData<T>(
    val version: String,
    val error: Boolean,
    val data: T,
    val messages: List<String> = ArrayList()
)
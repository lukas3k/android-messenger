package cz.siska.messenger.models

data class Message(
    val id: Int,
    val content: String,
    val chatRoom: ChatRoom,
    val createdBy: User,
    val created: String
)
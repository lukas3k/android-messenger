package cz.siska.messenger.messages

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.siska.messenger.MessengerApplication
import cz.siska.messenger.databinding.AuthorMessageListItemBinding
import cz.siska.messenger.databinding.MessageListItemBinding
import cz.siska.messenger.models.Message
import cz.siska.messenger.storage.SharedPrefManager
import kotlinx.android.synthetic.main.message_list_item.view.*

class MessageListItemAdapter : ListAdapter<Message, RecyclerView.ViewHolder>(DiffCallback) {

    class MessageViewHolder(private var binding: MessageListItemBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(message: Message) {
            binding.message = message
            binding.authorFirstLetter = message.createdBy.name.first().toString()
            binding.executePendingBindings()
        }
    }

    class AuthorMessageViewHolder(private var binding: AuthorMessageListItemBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(message: Message) {
            binding.message = message
            binding.authorFirstLetter = message.createdBy.name.first().toString()
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Message>() {
        override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            MessageViewHolder(MessageListItemBinding.inflate(LayoutInflater.from(parent.context)))
        } else {
            AuthorMessageViewHolder(AuthorMessageListItemBinding.inflate(LayoutInflater.from(parent.context)))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message = getItem(position)

        if (holder is MessageViewHolder) {
            holder.bind(message)
        }

        if (holder is AuthorMessageViewHolder) {
            holder.bind(message)
        }

        holder.itemView.tvMessageIcon.background.setTint(
            Color.parseColor("#" + message.createdBy.color)
        )
    }

    override fun getItemViewType(position: Int): Int {
        val message = getItem(position)
        val prefManager = SharedPrefManager.getInstance(MessengerApplication.instance)

        return if (message.createdBy.id == prefManager.user.id) {
            1
        } else {
            0
        }
    }
}
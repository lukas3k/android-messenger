package cz.siska.messenger.messages

import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.siska.messenger.databinding.FragmentMessageListBinding
import cz.siska.messenger.models.Message
import cz.siska.messenger.models.MessageCreate
import cz.siska.messenger.models.ResponseData
import cz.siska.messenger.network.Api
import cz.siska.messenger.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_message_list.*
import kotlinx.android.synthetic.main.fragment_message_list.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MessageListFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        val binding = FragmentMessageListBinding.inflate(inflater)
        val actionBar = ((activity as MainActivity).supportActionBar as ActionBar)
        val chatRoom = MessageListFragmentArgs.fromBundle(requireArguments()).selectedChatRoom
        val viewModelFactory = MessageListViewModelFactory(chatRoom)
        val viewModel = ViewModelProvider(
            this, viewModelFactory).get(MessageListViewModel::class.java)

        actionBar.title = chatRoom.title
        actionBar.setDisplayHomeAsUpEnabled(true)


        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.messageListItems.adapter = MessageListItemAdapter()

        binding.root.buttonSend.setOnClickListener {
            val messageContent = etMessage.text.toString().trim()
            if (messageContent.isNotEmpty()) {
                Api.retrofitService.createMessage(chatRoom.id, MessageCreate(messageContent))
                    .enqueue(object: Callback<ResponseData<Message>> {
                        override fun onResponse(
                            call: Call<ResponseData<Message>>,
                            response: Response<ResponseData<Message>>
                        ) {
                            if (response.isSuccessful) {
                                response.body()?.data?.let { it1 -> viewModel.prependMessage(it1) }
                                etMessage.text.clear()
                                hideKeyboard(etMessage)
                            }
                        }

                        override fun onFailure(call: Call<ResponseData<Message>>, t: Throwable) {
                            Toast.makeText(
                                activity,
                                t.message,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    })
            }
        }

        binding.root.swipeMessageLayout.setOnRefreshListener {
            viewModel.refreshMessages(chatRoom.id)
            binding.root.swipeMessageLayout.isRefreshing = false
        }

        return binding.root
    }

    private fun hideKeyboard(editText: EditText) {
        (activity?.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager).apply {
            hideSoftInputFromWindow(editText.windowToken, 0)
        }
    }
}
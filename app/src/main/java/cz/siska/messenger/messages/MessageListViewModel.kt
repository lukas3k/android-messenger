package cz.siska.messenger.messages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.siska.messenger.models.ChatRoom
import cz.siska.messenger.models.Message
import cz.siska.messenger.network.Api
import kotlinx.coroutines.launch

class MessageListViewModel(chatRoom: ChatRoom) : ViewModel() {
    private val _messages = MutableLiveData<List<Message>>()

    val messages: LiveData<List<Message>>
        get() = _messages

    init {
        refreshMessages(chatRoom.id)
    }

    fun refreshMessages(chatRoomId: Int) {
        viewModelScope.launch {
            try {
                _messages.value = Api.retrofitService.getAllMessages(chatRoomId).data
            } catch (e: Exception) {
                _messages.value = ArrayList()
            }
        }
    }

    fun prependMessage(message: Message) {
        val m = listOf(message) + _messages.value as List<Message>
        _messages.value = m
    }
}
package cz.siska.messenger.messages

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cz.siska.messenger.models.ChatRoom

class MessageListViewModelFactory(private val chatRoom: ChatRoom) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MessageListViewModel::class.java)) {
            return MessageListViewModel(chatRoom) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
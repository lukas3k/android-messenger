package cz.siska.messenger

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.siska.messenger.chatrooms.ListItemAdapter
import cz.siska.messenger.messages.MessageListItemAdapter
import cz.siska.messenger.models.ChatRoom
import cz.siska.messenger.models.Message

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<ChatRoom>?) {
    val adapter = recyclerView.adapter as ListItemAdapter
    adapter.submitList(data)
}

@BindingAdapter("messageListData")
fun bindMessageRecyclerView(recyclerView: RecyclerView, data: List<Message>?) {
    val adapter = recyclerView.adapter as MessageListItemAdapter
    adapter.submitList(data)
}